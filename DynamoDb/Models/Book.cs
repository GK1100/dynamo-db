﻿using System.Collections.Generic;
using Amazon.DynamoDBv2.DataModel;

namespace DynamoDb.Models
{
    [DynamoDBTable("Books")]
    public class Book
    {
        [DynamoDBHashKey]
        public int BookId { get; set; }

        [DynamoDBProperty]
        public string Title { get; set; }

        [DynamoDBProperty] 
        public List<string> Authors { get; set; }
    }
}