﻿using System.Threading.Tasks;
using DynamoDb.Models;
using DynamoDb.Services;
using Microsoft.AspNetCore.Mvc;

namespace DynamoDb.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BooksController : ControllerBase
    {
        private readonly BookService _bookService;

        public BooksController(BookService bookService)
        {
            _bookService = bookService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _bookService.GetAllAsync());
        }
        
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _bookService.GetAsync(id));
        }
        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Book book)
        {
            await _bookService.CreateAsync(book);
            return Ok();
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Book book)
        {
            await _bookService.UpdateAsync(id, book);
            return Ok();
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _bookService.DeleteAsync(id);
            return Ok();
        }
    }
}