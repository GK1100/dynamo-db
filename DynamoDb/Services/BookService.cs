﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using DynamoDb.Models;

namespace DynamoDb.Services
{
    public class BookService
    {
        private readonly DynamoDBContext _context;
        public BookService(DynamoDBContext context)
        {
            _context = context;
        }

        public async Task<Book> GetAsync(int id)
        {
            Book book = await _context.LoadAsync<Book>(id);
            return book;
        }

        public async Task<List<Book>> GetAllAsync()
        {
            List<Book> books = await _context.ScanAsync<Book>(new List<ScanCondition>()).GetRemainingAsync();
            return books;
        }

        public async Task CreateAsync(Book book)
        {
            await _context.SaveAsync(book);
        }

        public async Task UpdateAsync(int id, Book newBook)
        {
            Book book = await GetAsync(id);
            book.Title = newBook.Title;
            book.Authors = newBook.Authors;

            await _context.SaveAsync(book);
        }

        public async Task DeleteAsync(int id)
        {
            await _context.DeleteAsync<Book>(id);
        }
    }
}